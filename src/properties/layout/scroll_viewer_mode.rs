use super::ScrollMode;

/// The `ScrollViewerMode` struct is used to define the vertical and horizontal scroll behavior of the `ScrollViewer`.
#[derive(Default)]
pub struct ScrollViewerMode {
    /// Vertical scroll mode.
    pub vertical: ScrollMode,

    /// Horizontal scroll mode.
    pub horizontal: ScrollMode,
}

property!(
    ScrollViewerMode,
    ScrollViewerModeProperty,
    scroll_viewer_mode,
    shared_scroll_viewer_mode
);

impl ScrollViewerMode {
    /// Creates a new scroll viewer mode.
    pub fn new(vertical: ScrollMode, horizontal: ScrollMode) -> Self {
        ScrollViewerMode {
            vertical,
            horizontal,
        }
    }
}
