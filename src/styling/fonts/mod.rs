/// Reference to the material icon font.
pub static MATERIAL_ICONS_REGULAR_FONT: &'static [u8; 128180] =
    include_bytes!("MaterialIcons-Regular.ttf");
pub static ROBOTO_REGULAR_FONT: &'static [u8; 145348] = include_bytes!("Roboto-Regular.ttf");
